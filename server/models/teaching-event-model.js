import {EdmMapping, EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import {Args} from "@themost/common/utils";
import {DataConflictError} from "../errors";
let EducationEvent = require('./education-event-model');
/**
 * @class
 
 * @property {CourseClass|any} courseClass
 * @property {Array<CourseClassSection|any>} sections
 * @property {number} id
 * @property {number} eventAttendanceCoefficient
 * @augments {DataObject}
 */
@EdmMapping.entityType('TeachingEvent')
class TeachingEvent extends EducationEvent {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.action('close', 'TeachingEvent')
    async closeEvent(){
        return this.context.model('TeachingEvent').save({
            ...this,
            eventStatus: await this.context.model('EventStatusTypes').where('alternateName').equal('EventCompleted').getItem()
        });
    }

    @EdmMapping.action('open', 'TeachingEvent')
    async openEvent(){
        return this.context.model('TeachingEvent').save({
            ...this,
            eventStatus: await this.context.model('EventStatusTypes').where('alternateName').equal('EventOpened').getItem()
        });
    }

    @EdmMapping.param('records', EdmType.CollectionOf('TeachingEventAttendance'), false, true)
    @EdmMapping.action('attendance', EdmType.CollectionOf('TeachingEventAttendance'))
    async setAttendanceRecords(records) {
        const _event = await this.context.model('TeachingEvent').where('id').equal(this.getId()).expand('eventStatus').getItem();
        if(_event.hasOwnProperty('eventStatus') && _event.eventStatus.hasOwnProperty('alternateName') && _event.eventStatus.alternateName === 'EventCompleted') {
            throw new DataConflictError('You cannot add attendance records to a completed event.', null, 'TeachingEvent');
        }
        const finalRecords = [];
        Args.check(Array.isArray(records) || typeof records === 'object', new DataConflictError('Records should either be an array or an object of records', null, 'EducationEventAttendance'));
        if (Array.isArray(records)){
            // check if every item in array has the same coefficient
            if((new Set(records.map(x => x.hasOwnProperty('coefficient') && x.coefficient))).size > 1)
                throw new DataConflictError('Not all records have the same coefficient', 'Make sure that every record has the same coefficient', 'EducationEventAttendance');
            for (let [index, record] of records.entries()) {
                if (record.hasOwnProperty('coefficient') && _event.hasOwnProperty('eventAttendanceCoefficient') && _event.eventAttendanceCoefficient !== record.coefficient) {
                    // check if all records have the same coefficient as the first element if the coefficient in the attendance record doesn't match the one in the event.
                    // checks only once to save some resources even though the check for the index is performed in every iteration, it isn't as expensive.
                    if (index === 0 && !records.every(x => x.coefficient === record.coefficient))
                        throw new DataConflictError('Coefficient values between attendance record and event don\'t match.', `Check the record coefficient of student ${record.student}`, 'TeachingEvent');
                }
                record.event = {..._event};
                finalRecords.push(record);
            }
            // check for duplicate student records
            if(!((new Set(records.map(x => x.student))).size === finalRecords.length)){
                throw new DataConflictError('Multiple records for student was found. Make sure there are no duplicate entries', null, 'EducationEventAttendance');
            }
        } else {
            // here the records is object and it is guaranteed by the Args.check() at the beginning of the method.
            if (records.hasOwnProperty('coefficient') && _event.hasOwnProperty('eventAttendanceCoefficient') && _event.eventAttendanceCoefficient !== records.coefficient) {
                throw new DataConflictError('Coefficient between attendance record and event don\'t match.', null, 'TeachingEvent');
            }
            finalRecords.push(records);
        }
        return this.context.model(_event.presenceType ===1 ? 'TeachingEventPresence' : 'TeachingEventAbsence').save(finalRecords);
    }

    @EdmMapping.func("attendance", EdmType.CollectionOf("StudentCourseClass"))
    async getAttendanceRecords() {
        const event = await this.context.model('TeachingEvent').where('id').equal(this.getId()).expand('sections').getTypedItem();
        return event.sections.length ? this.context.model('StudentCourseClass').where('courseClass').equal(event.courseClass)
                .and('section').in(event.sections.map(x => x.section))
                .expand({
                    "name": "attendances",
                    "options": {
                        "$filter": `event eq ${this.getId()}`
                    }
                }).select('TeachingEventClassStudents').prepare()
            : this.context.model('StudentCourseClass').where('courseClass').equal(event.courseClass)
                .expand({
                    "name": "attendances",
                    "options": {
                        "$filter": `event eq ${this.getId()}`
                    }
                }).select('TeachingEventClassStudents').prepare();
    }
}
module.exports = TeachingEvent;
