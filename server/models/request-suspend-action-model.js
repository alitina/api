import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataNotFoundError} from '@themost/common';
import {DataConflictError} from "../errors";
let RequestAction = require('./request-action-model');
/**
 * @class

 * @property {number} id
 */
@EdmMapping.entityType('RequestSuspendAction')
class RequestSuspendAction extends RequestAction {
    /**
     * @constructor
     */
    constructor() {
        super();
        // this event is going to be used to handle claim operation and
        // create a StudentRemoveAction where the initiator is this action
        // and owner is the agent that claimed this action
        this.on('claimed', (event, cb) => {
            (async () => {
                // try to get action by initiator
                const item = await this.context.model('StudentSuspendAction')
                    .where('initiator').equal(this.getId())
                    .getItem();
                // validate action state
                if (item == null) {
                    // convert current action
                    const newItem = await this.context.model('RequestSuspendAction')
                        .where('id').equal(this.getId())
                        .select(
                            'student as object',
                            'agent as owner',
                            'suspensionPeriod',
                            'suspensionYear',
                            'suspensionPeriods',
                            'suspensionType'
                            )
                        .getItem();
                    // validate that item exists
                    if (newItem == null) {
                        throw new DataNotFoundError('The current action cannot be found or is inaccessible', null, 'RequestRemoveAction');
                    }
                    // validate owner
                    if (newItem.owner == null) {
                        throw new DataConflictError('The operation cannot be completed because initiator agent cannot be found or has not be set yet.', null, 'RequestRemoveAction');
                    }
                    Object.assign(newItem, {
                        initiator: this.getId(), // set initiator (this action)
                        actionStatus: { // set action status to active
                            alternateName: 'ActiveActionStatus'
                        }
                    });
                    // and finally save action
                    await this.context.model('StudentSuspendAction').save(newItem);
                }
            })().then(() => {
               return cb();
            }).catch((err) => {
                return cb(err);
            });

        });
    }

    @EdmMapping.func('initiatorOf', EdmType.CollectionOf('StudentSuspendAction'))
    async initiatorOf() {
        return this.context.model('StudentSuspendAction')
            .where('initiator').equal(this.id)
            .and('initiator').notEqual(null)
            .prepare();
    }
}
module.exports = RequestSuspendAction;
