import {DataObject} from "@themost/data/data-object";
import {EdmMapping} from "@themost/data/odata";
import {NodeDataService} from "@themost/node";
import {DataError, DataNotFoundError, HttpError} from "@themost/common";
import unirest from "unirest";
import {URL} from "url";
import { DataConflictError } from "../errors";

@EdmMapping.entityType('ServiceProvider')
/**
 * @class
 * @augments {DataObject}
 */
class ServiceProvider extends DataObject {
    constructor() {
        super();
    }

    async execute(options) {
        const serviceProvider = await this.context.model('ServiceProvider').where('id').equal(this.getId())
            .expand('authorization').getItem();
        if (serviceProvider == null) {
            throw new DataNotFoundError('The service provider cannot be found or is inaccessible.');
        }
        options.headers = options.headers || {};

        // check if the source has some keychain identifier
        let keychainItem = null;
        if (serviceProvider.authenticator) {
            // and validate it
            const keychain = this.context.getConfiguration().getSourceAt('settings/universis/keychain' || []);
            // first of all, validate keychain existance
            if (!(Array.isArray(keychain) && keychain.length)) {
                throw new DataError('E_KEYCHAIN_CONFIG', 'The process cannot continue due to invalid keychain configuration.'
                );
            }
            // then try to find the specified identifier
            keychainItem = keychain.find(
                (item) => item.identifier === serviceProvider.authenticator);
            if (keychainItem == null) {
                throw new DataError(
                    'E_KEYCHAIN_ITEM',
                    'The specified keychain identifier cannot be found inside the keychain configuration',
                    null,
                    'ServiceProvider',
                    'authenticator'
                );
            }
        }
        // load service provider
        if (serviceProvider.authorization && serviceProvider.authorization.alternateName === 'bearer') {
            const data = {
                'grant_type': keychainItem.grant_type,
                'scope': keychainItem.scope,
                'username': keychainItem.username,
                'password': keychainItem.password,
                'client_id': keychainItem.client_id,
                'client_secret': keychainItem.client_secret
            };
            const response = await this.authorize(keychainItem.tokenURL, data);

            Object.assign(options.headers, {'Authorization': 'Bearer ' + response.access_token});
            const finalService = new NodeDataService(serviceProvider.url);
            return await finalService.execute(options);
        }
        if (serviceProvider.authorization && serviceProvider.authorization.alternateName === 'apikey') {
            Object.assign(options.headers, {'apikey': keychainItem.apikey});
            const finalService = new NodeDataService(serviceProvider.url);
            return await finalService.execute(options);
        }
        if (serviceProvider.authorization && serviceProvider.authorization.alternateName === 'header') {
            if (!( keychainItem
                && keychainItem.username
                && keychainItem.password
                && keychainItem.authorizeURL
                && keychainItem.headerKey )) {
                    throw new DataConflictError('Missing at least one of the following attributes in the keychain config (username, password, authorizeURL, headerKey).');
                }
            const authorizeUser = {
                username: keychainItem.username,
                password: keychainItem.password
            };
            const headerKey = keychainItem.headerKey;
            // get a token from the specified header
            const authorizationToken = await this.authorizeByHeader(
                keychainItem.authorizeURL,
                authorizeUser,
                headerKey
                );
            // validate it
            if (authorizationToken == null) {
                throw new DataConflictError(`The service reached ${keychainItem.authorizeURL} successfully, but did not manage to extract a token from a header key named ${headerKey}.`, null, 'ServiceProvider');
            }
            // assign it to existing headers
            if (Object.prototype.hasOwnProperty.call(options.headers, headerKey) === false) {
                Object.defineProperty(options.headers, headerKey, {
                    // only enumerable
                    enumerable: true,
                    configurable: false,
                    writable: false,
                    value: authorizationToken
                });
            }
            // and proceed with executing the request
            const dataService = new NodeDataService(serviceProvider.url);
            return await dataService.execute(options);
        }
        throw new DataConflictError('The specified authorization method is not yet implemented or it is empty.', null, 'ServiceProvider');
    }

    authorize(tokenUrl, authorizeUser) {
        return new Promise((resolve, reject)=> {
            return unirest.post(new URL('', tokenUrl))
                .type('form')
                .send(authorizeUser).end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

    authorizeByHeader(authorizeUrl, authorizeUser, headerKey) {
        return new Promise((resolve, reject) => {
            return unirest.post(new URL(authorizeUrl).toString())
                .type('form')
                .send(authorizeUser)
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.headers[headerKey]);
                });
        });
    }
}

module.exports = ServiceProvider;
