import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {*} additionalType
 * @property {string} urlTemplate
 * @property {string} actionApplication
 * @property {string} application
 * @property {string} actionPlatform
 * @property {string} httpMethod
 * @property {string} encodingType
 * @property {string} contentType
 * @property {number} id
 * @property {string} alternateName
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 * @augments {DataObject}
 */
@EdmMapping.entityType('EntryPoint')
class EntryPoint extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = EntryPoint;