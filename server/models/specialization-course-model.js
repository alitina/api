import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
const Rule = require('./rule-model');
/**
 * @class
 
 * @property {string} id
 * @property {StudyProgramCourse|any} studyProgramCourse
 * @property {StudyProgramSpecialty|any} specialization
 * @property {number} specializationIndex
 * @property {Semester|any} semester
 * @property {number} coefficient
 * @property {number} units
 * @property {number} ects
 * @property {number} courseType
 * @property {number} sortIndex
 * @property {string} identifier
 * @property {Date} dateModified
 */
@EdmMapping.entityType('SpecializationCourse')
class SpecializationCourse extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
    @EdmMapping.func('ProgramCourseRegistrationRules', EdmType.CollectionOf('Rule'))
    async getProgramCourseRegistrationRules() {

        // get study program course
        let studyProgramCourse;
        // if studyProgramCourse is empty
        if (this.studyProgramCourse == null) {
            // fetch values
            this.studyProgramCourse =  await this.getModel().where('id').equal(this.getId())
                .select('studyProgramCourse').silent().value();
        }
        /**
         * get study program course
         * @type {StudyProgramCourse}
         */
        studyProgramCourse = this.context.model('StudyProgramCourse').convert(this.studyProgramCourse);
        // and return rules of type ProgramCourseRegistrationRule
        return studyProgramCourse.getProgramCourseRegistrationRules();
    }
}
module.exports = SpecializationCourse;
