import {EdmMapping,EdmType} from '@themost/data/odata';
import Action = require('./action-model');

/**
 * @class
 */
declare class OrganizeAction extends Action {

     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 

}

export = OrganizeAction;