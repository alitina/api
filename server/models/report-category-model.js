import {EdmMapping, DataObject} from '@themost/data';

/**
 * @class
 * @property {number} id
 * @property {string} additionalType
 * @property {string} appliesTo
 * @property {string} alternateName
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 */
@EdmMapping.entityType('ReportCategory')
class ReportCategory extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = ReportCategory;
