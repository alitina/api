import express from 'express';
import {HttpNotFoundError} from '@themost/common';
import {multerInstance} from './multer';
import {TraceUtils} from '@themost/common';
import path from 'path';
/**
 * @param {ConfigurationBase} configuration
 * @return Router
 */
function messagesRouter(configuration) {
    let router = express.Router();
    // set user storage from application configuration
    const upload = multerInstance(configuration);
    upload.storage.getDestination(null, null, (err, destination) => {
        if (err) {
            TraceUtils.error(`Routes: An error occurred while initializing messages router.`);
            return TraceUtils.error(err);
        }
        TraceUtils.info(`Routes: Messages router starts using "${path.resolve(destination)}" as user storage.`);
    });
    router.post('/:id/attachments/add', upload.single('file'),async function addAttachment (req, res, next) {
        try {
            // get studentMessage
            let studentMessage = await req.context.model('StudentMessage').where('id').equal(req.params.id).select('id').getTypedItem();
            if (typeof studentMessage === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError('Student message cannot be found'));
            }
            studentMessage.addAttachment(req.file).then(result=>{
                res.json(result);
            }).catch(err=>{
                return next(err);
            });

        } catch (err) {
            return next(err);
        }
    });
    router.post('/:id/attachments/:attachment/remove',async function removeAttachment (req, res, next) {
        try {
            // get studentMessage
            let studentMessage = await req.context.model('StudentMessage').where('id').equal(req.params.id).select('id').getTypedItem();
            if (typeof studentMessage === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError('Student message cannot be found'));
            }
            studentMessage.removeAttachment(req.params.attachment).then(result=>{
                res.json(result);
            }).catch(err=>{
                return next(err);
            });

        } catch (err) {
            return next(err);
        }
    });
    return router;
}

export {messagesRouter};
