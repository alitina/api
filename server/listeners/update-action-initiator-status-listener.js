import {DataConflictError} from "../errors";
// eslint-disable-next-line no-unused-vars
import {DataEventArgs} from "@themost/data";

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    /**
     * @type {ActionStatusType}
     */
    let previousActionStatus = null;
    const context = event.model.context;
    if (event.state === 1) {
        return;
    }
    const initiator = await event.model
        .where('id').equal(event.target.id)
        .select('initiator')
        .expand('initiator')
        .silent()
        .value();
    if (initiator == null) {
        return;
    }
    if (event.previous == null) {
        throw new Error('The previous state of action cannot be determined.');
    }
    // get previous agent
    previousActionStatus = await context.model('ActionStatusType').find(event.previous.actionStatus).getItem();
    if (previousActionStatus == null) {
        throw new DataConflictError('Previous action status cannot be found or is inaccessible.');
    }
    // get current actionStatus
    let currentActionStatus = await event.model.where('id').equal(event.target.id).select('actionStatus').value();
    if (currentActionStatus == null) {
        throw new DataConflictError('Action status cannot be found or is inaccessible.');
    }

    // if agent is empty
    if (previousActionStatus.alternateName !== currentActionStatus.alternateName) {
        // update initiator with status equal to current
        const updateInitiator = {
            id: initiator.id,
            actionStatus: currentActionStatus
        };
        await context.model(initiator.additionalType).silent().save(updateInitiator);
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
