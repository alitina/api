import {DataModel} from "@themost/data";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return beforeRemoveAsync(event).then( () => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 */
export async function beforeRemoveAsync(event) {
    const model = event.model;
    const getReferenceMappings = DataModel.prototype.getReferenceMappings;
    model.getReferenceMappings = async function () {
        const res = await getReferenceMappings.bind(this)();
        // remove readonly model CourseAreaRuleExCheckValues, StudentAvailableClass from mapping before delete for models CourseArea, CourseSector
        const mappings = ['CourseAreaRuleExCheckValues','StudentAvailableClass','CourseSectorRuleExCheckValues'];
        return res.filter((mapping) => {
            return mappings.indexOf(mapping.childModel) < 0;
        });
    };
}
