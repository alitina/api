import {
	DataError,
	DataNotFoundError,
	TraceUtils
} from "@themost/common";
import { DataObjectState } from "@themost/data";
import { DataConflictError } from "../errors";


/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	if (event.state !== DataObjectState.Insert) {
		return;
	}
	let errorMessage;
	const context = event.model.context;
	const studentId =
		typeof event.target.student === "object"
			? event.target.student.id
			: event.target.student;
	// get student
	const student = await context
		.model("Student")
		.where("id")
		.equal(studentId)
		.select(
			"id",
			"user",
			"department",
			"person",
			"studentIdentifier",
			"inscriptionYear"
		)
		.expand(
			{
				name: "person",
				options: {
					$expand: "locales",
				},
			}, 'department'
		)
		.getTypedItem();
	if (student == null) {
		errorMessage = "The specified student cannot be found or is inaccessible.";
		throw new DataNotFoundError(errorMessage);
	}
	// ensure that student is not linked to a user
	if (student.user != null) {
		errorMessage = `The student is already linked to a user with id ${student.user}.`;
		throw new DataConflictError(errorMessage);
	}
	// get department configuration (avoid caching through Department)
	const departmentConfiguration = await context.model('DepartmentConfiguration')
		.where('department').equal(student.department && student.department.id)
		.select('id', 'studentUsernameFormat', 'studentUsernameIndex')
		.silent()
		.getItem();
	// get username format and validate it
	const usernameFormat =
		departmentConfiguration &&
		departmentConfiguration.studentUsernameFormat;
	if (usernameFormat == null) {
		errorMessage = `Cannot create user for student with id ${student.id}, because studentUsernameFormat is not set for department [${student.department.id}]-${student.department.name}.`;
		throw new DataError("E_FORMAT_MISSING", errorMessage);
	}
	// get username index
	const usernameIndex =
		departmentConfiguration.studentUsernameIndex;
	let username;
	try {
		// create new username
		username = student.generateUsername(usernameFormat, usernameIndex, student);
	} catch (err) {
		errorMessage = err.message;
		TraceUtils.error(err);
		throw new DataError("E_GENERATE_USERNAME", errorMessage);
	}
	// check if a user with that username already exists
	let exists = await context
		.model("User")
		.where("name")
		.equal(username)
		.select("id")
		.silent()
		.count();

	if (exists) {
		/* To fully cover uniqueness for all combinations, format elements have to be handled
        differently if they give the same results at each generation to avoid infinite loops.
        Format elements that are "static" and can produce infinite loops: 'T','G','F','Y'
        Student identifier format 'S' is considered static in edge cases, as well as the index 'I'.
        Format elements that are random or static but different for each student: 'U'*/
		const dynamicElements = ["U"];
		const containsDynamicElements = usernameFormat
			.split(";")
			.find((formatElement) => dynamicElements.includes(formatElement));
		// If the username format does not contain any dynamic elements, a number has to be appended at the end to force uniqueness.
		// This is a safe way to not interfere with the username itself in any way.
		if (!containsDynamicElements) {
			let index = 0;
			do {
				index++;
				exists = await context
					.model("User")
					.where("name")
					.equal(username + index.toString())
					.select("id")
					.silent()
					.count();
			} while (exists);
			// append the final index
			username += index.toString();
		} else {
			// format contains at least one dynamic element, so recreate a username
			do {
				username = student.generateUsername(usernameFormat, usernameIndex, student);
				exists = await context
					.model("User")
					.where("name")
					.equal(username)
					.select("id")
					.silent()
					.count();
			} while (exists);
		}
	}
	// get students group
	const studentsGroup = await context
		.model("Group")
		.where("name")
		.equal("Students")
		.select("id")
		.silent()
		.getItem();
	if (studentsGroup == null) {
		errorMessage = "Students group is missing";
		throw new DataNotFoundError(errorMessage);
	}
	// create a new user and assign students group
	const newUser = {
		name: username,
		email: student.person.email,
		description: `${student.person.familyName} ${student.person.givenName}`,
		groups: [
			{
				id: studentsGroup.id,
			},
		],
		departments: [student.department]
	};
	// save new user
	const user = await context.model("UserReference").silent().save(newUser);
	// assign to student
	student.user = user;
	// and update
	Object.assign(student, {
		$state: 2,
	});
	await context.model("Student").save(student);
	// increment username index
	departmentConfiguration.studentUsernameIndex += 1;
	// and update it
	await context.model("DepartmentConfiguration").save(departmentConfiguration);
	// and finally update (complete) the action
	event.target.actionStatus = {
		alternateName: 'CompletedActionStatus'
	};
	await context
		.model("CreateStudentUserAction")
		.silent()
		.save(event.target);
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	return afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}
