/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */


import {GenericDataConflictError} from "../errors";

export function beforeSave(event, callback) {
    return GraduationEventUpdateListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return GraduationEventUpdateListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

class GraduationEventUpdateListener {

    static async beforeSaveAsync(event) {
        const target = event.model.convert(event.target);
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        if (event.state !== 4) {
            // check dates
            if (event.target.validThrough && event.target.validFrom) {
                if (event.target.validThrough < event.target.validFrom) {
                    throw new GenericDataConflictError('E_EVENT_INVALID_DATES', context.__('Start date cannot be greater than end date'), null, 'GraduationEvent');
                }
            }
            let organizer = event.target.organizer;
            if (event.state === 2) {
                organizer = await context.model('GraduationEvent').select('organizer').where('id').equal(event.target.id).value();
                if (event.target.organizer && organizer != event.target.organizer) {
                    //cannot change organizer
                    throw new GenericDataConflictError('E_EVENT_ORGANIZER', context.__('Cannot change organizer of the event'), null, 'GraduationEvent');
                }
            }
            // check if graduationEvent has studyPrograms from another department
            if (target.studyPrograms) {
                if (Array.isArray(target.studyPrograms) && target.studyPrograms.length > 0) {
                    //load study programs and check department
                    const studyPrograms = await context.model('StudyPrograms').select('id').where('department').equal(organizer).getItems();
                    target.studyPrograms.forEach(studyProgram => {
                        const isValid = studyPrograms.find((x) => {
                            return x.id === studyProgram.id;
                        });
                        if (!isValid) {
                            throw new GenericDataConflictError('E_EVENT_INVALID_PROGRAM', context.__('Invalid program. Study programs of a graduation event should refer to same department'), null, 'GraduationEvent');
                        }
                    });
                }
            }
        }

    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
    }

    /**
     * @param {DataEventArgs} event
     */
    static async beforeRemoveAsync(event) {
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        // TODO: check student graduation requests attachments
    }
}
