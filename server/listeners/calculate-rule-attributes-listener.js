// eslint-disable-next-line no-unused-vars
import {DataEventArgs} from "@themost/data";
import {DataError} from "@themost/common/errors";
import {LangUtils} from "@themost/common/utils";
import util from "util";

const RULES_REFERENCES = [
    {
        "refersToIdentifier": 10,
        "refersTo": "Student"
    },
    {
        "refersToIdentifier": 20,
        "refersTo": "CourseArea"
    },
    {
        "refersToIdentifier": 30,
        "refersTo": "Course"
    },
    {
        "refersToIdentifier": 40,
        "refersTo": "CourseType"
    },
    {
        "refersToIdentifier": 50,
        "refersTo": "ProgramGroup"
    },
    {
        "refersToIdentifier": 60,
        "refersTo": "Thesis"
    },
    {
        "refersToIdentifier": 70,
        "refersTo": "Internship"
    },
    {
        "refersToIdentifier": 80,
        "refersTo": "MeanGrade"
    },
    {
        "refersToIdentifier": 90,
        "refersTo": "YearMeanGrade"
    },
    {
        "refersToIdentifier": 100,
        "refersTo": "RegisteredCourse"
    },
    {
        "refersToIdentifier": 110,
        "refersTo": "CourseCategory"
    },
    {
        "refersToIdentifier": 120,
        "refersTo": "CourseSector"
    },
    {
        "refersToIdentifier": 130,
        "refersTo": "SectionCategory"
    }
];

const RULES_TYPES = [
    {
        "targetTypeIdentifier": 101,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": "ProgramInscriptionRule"
    },
    {
        "targetTypeIdentifier": 102,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": "SpecialtyRule"
    },
    {
        "targetTypeIdentifier": 103,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": "GraduateRule"
    },
    {
        "targetTypeIdentifier": 103,
        "additionalTypeIdentifier": 8,
        "targetType": "Student",
        "additionalType": "GraduateRule"
    },
    {
        "targetTypeIdentifier": 104,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": "ThesisRule"
    },
    {
        "targetTypeIdentifier": 105,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": "InternshipRule"
    },
    {
        "targetTypeIdentifier": 106,
        "additionalTypeIdentifier": 4,
        "targetType": "ProgramCourse",
        "additionalType": "ProgramCourseRegistrationRule"
    },
    {
        "targetTypeIdentifier": 106,
        "additionalTypeIdentifier": 14,
        "targetType": "CourseClass",
        "additionalType": "ClassRegistrationRule"
    },
    {
        "targetTypeIdentifier": 107,
        "additionalTypeIdentifier": 3,
        "targetType": "ProgramGroup",
        "additionalType": "GroupRegistrationRule"
    },
    {
        "targetTypeIdentifier": 111,
        "additionalTypeIdentifier": 14,
        "targetType": "CourseClass",
        "additionalType": "SectionRegistrationRule"
    },
    {
        "targetTypeIdentifier": 109,
        "additionalTypeIdentifier": 13,
        "targetType": "Scholarship",
        "additionalType": "ScholarshipRule"
    },
    {
        "targetTypeIdentifier": 112,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": null
    }
]

/**
 *
 * @param {DataEventArgs} event
 * @returns {Promise<void>}
 */
export async function beforeSaveAsync(event) {
    let target = event.target;
    if (target.targetType && target.additionalType) {
        const findItem = RULES_TYPES.find((item) => {
            return item.targetType === target.targetType && item.additionalType === target.additionalType;
        });
        if (findItem) {
            target.targetTypeIdentifier = findItem.targetTypeIdentifier;
            target.additionalTypeIdentifier = findItem.additionalTypeIdentifier;
        } else {
            // throw error
            throw new DataError('E_RULE_TYPE', 'The specified rule type cannot be found.', null, 'Rule', 'targetType');
        }
    }
    if (target.refersTo) {
        const findItem = RULES_REFERENCES.find((item) => {
            return item.refersTo === target.refersTo;
        });
        // add -1 value to checkValues if refersTo is CourseType or Thesis
        if (target.refersTo === 'CourseType' || target.refersTo === 'Thesis') {
            if ((target.checkValues && target.checkValues.length === 0) || !target.checkValues) {
                target.checkValues = '-1';
            }
        }
        // merge values of totalCourses and totalCoursesPerType refersTo is YearMeanGrade
        if (target.refersTo === 'YearMeanGrade' || target.refersTo === 'MeanGrade') {
            target = convertFromYearMeanGrade(target);
        }
        if (findItem) {
            target.refersToIdentifier = findItem.refersToIdentifier;
        } else {
            throw new DataError('E_RULE_REFERENCE', 'The specified rule reference cannot be found.', null, 'Rule', 'refersTo');
        }
    }
    // set ruleExpression to null for empty string
    if (target.hasOwnProperty('ruleExpression')) {
        if (target.ruleExpression === '') {
            target.ruleExpression = null;
        }
    }
}

function convertFromYearMeanGrade(target) {

    if ((target.value5 && target.value5.length === 0) || !target.value5) {
        throw new DataError('E_RULE_INVALID_VALUE', 'Total courses field is required.', null, 'Rule', 'totalCourses');
    } else {
        if (target.value5 && target.value5.length > 0) {
            for (let i = 0; i < target.value5.length; i++) {
                const courseType = target.value5[i];
                target.value5[i] = `${courseType.courseType.id};${courseType.courses}`
            }
            target.value5 = target.value5.join('#');
        }
        if (target.value12 && target.value12.length > 0) {
            for (let i = 0; i < target.value12.length; i++) {
                const courseType = target.value12[i];
                target.value12[i] =courseType.courses>0? `${courseType.courseType.id};${courseType.courses}`: null;
            }
            target.value12 =target.value12? target.value12.join('#'):null;
        } else {
            target.value12 = null;
        }
        // calculate ruleCheckType value as powers of two
        let value4 = 0;
        for (let i = 0; i < target.value4.length; i++) {
            const ruleCheckType = target.value4[i];
            value4 += Math.pow(2, LangUtils.parseFloat(ruleCheckType.id) - 1);
        }
        target.value4 = value4;
        // add -1 value to periods
        if (!target.value6) {
            target.value6 = '-1';
        }
    }
    if (target.refersTo === 'MeanGrade') {
        // convert semesters
        if (target.checkValues && target.checkValues.length > 0) {
            target.checkValues = target.checkValues.map(function (x) {
                return x.id;
            }).join(',');
        } else {
            target.checkValues = -1;
        }
        if (target.value1 && target.value1.length > 0) {
            target.value1 = target.value1.map(x=>{
                return x.id;
            }).join(',');
        } else {
            target.value1 = null;
        }
        if (target.value2 && target.value2.length > 0) {
            target.value2 = target.value2.map(x=>{
                return x.id;
            }).join(',');
        } else {
            target.value2 = null;
        }
    }
    return target;
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback()
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return callback();
}
