import _ from 'lodash';
import {TraceUtils, LangUtils} from '@themost/common/utils';
import util from "util";
import Rule from "./../models/rule-model";

/**
 * @class
 * @augments Rule
 */
export default class RegisteredCourseRule extends Rule {
    constructor(obj) {
        super('Rule', obj);
    }

    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {function(Error=,*=)} done
     */
    validate(obj, done) {
        try {
            //done(null, new ValidationResult(false,'FAIL','Class Registration was cancelled by the user'));
            const self = this;
            const context = self.context;
            const students = context.model('Student'), student = students.convert(obj.student), studentCourseClass = context.model('StudentCourseClass');
            if (_.isNil(student)) {
                return done(null, self.failure('ESTUD', 'Student data is missing.'));
            }
            if (_.isNil(student.getId())) {
                return done(null, self.failure('ESTUD', 'Student data cannot be found.'));
            }
            if (LangUtils.parseInt(this.value6)===1) {
                return done(null, self.success('SUCC', 'The rule is disabled.'));
            }

            //get queryable object
            const values = this.checkValues.split(',');
            const q = studentCourseClass.where('student').equal(student.getId()).and('course/id').in(values).prepare();

            //find previous periods
            let prevPeriod= 0;
            let prevYear= context.model('AcademicYear').convert(obj.registrationYear).getId();
            if (context.model('AcademicPeriod').convert(obj.registrationPeriod).getId()===2) {
                prevPeriod = 1;
            }
            else {
                prevPeriod = 2;
                prevYear = prevYear - 1;
            }
            const checkType = this.value6;
            let numberToCheck=0;
            switch (checkType) {
                case "0":
                    //student class must not be found in any previous period
                    break;
                case "-2":
                    //student class must be found in the previous period
                    q.where('registration/registrationYear').equal(prevYear).and('registration/registrationPeriod').equal(prevPeriod).prepare();
                    numberToCheck = 1;
                    break;
                case "2":
                    //student class must not be found in the previous period
                    q.where('registration/registrationYear').notEqual(prevYear).and('registration/registrationPeriod').notEqual(prevPeriod).prepare();
                    numberToCheck = 0;
                    break;
                default:
                    //(-1): student class must be found in any previous period
                    numberToCheck = 1;
            }
            let totalCourses=values.length;
            if (LangUtils.parseInt( self.value4)!==0) {
                totalCourses = self.value4;
            }
            self.excludeStudent(student, function (err, exclude) {
                if (err) {
                    TraceUtils.error(err);
                    return done(null, self.failure('EFAIL', 'An error occured while trying to validate rules.', 'An occured while searching excluded students.'));
                }
                if (exclude) {
                    return done(null, self.success('SUCC', 'The input data meets the specified rules.', 'Student was excluded from rule validation due to specific attributes.'));
                }
                else {
                    q.silent().select(['course', 'count(course) as res']).groupBy("course").flatten().all(function (err, result) {
                        if (err) {
                            TraceUtils.error(err);
                            return done(null, self.failure('EFAIL', 'An error occured while trying to validate rules.'));
                        }
                        let numOfSuccess = 0;
                        for (let i = 0; i < values.length; i++) {
                            const obj1 = values[i];
                            const v = result.find(function (y) {
                                return y.course.toString() === obj1;
                            });
                            if (v) {
                                if (LangUtils.parseFloat(v.res) >= numberToCheck && numberToCheck>0)
                                    numOfSuccess += 1;
                            }
                            else {//not found
                                if (numberToCheck === 0)
                                    numOfSuccess += 1;
                            }
                        }
                        // keep result in data and pass this to validationResult
                        const data= {
                            "result": numOfSuccess,
                            "operator": 'equal',
                            "value1": totalCourses
                        };
                        if (numOfSuccess >=totalCourses) {
                            self.formatDescription(function(err, message) {
                                if (err) { return done(err); }
                                return done(null, self.success('SUCC',message,null,data));
                            });
                        }
                        else {
                            self.formatDescription(function(err, message) {
                                if (err) { return done(err); }
                                return done(null, self.failure('FAIL',message,null,data));
                            });
                        }
                    });
                }
            });
        }
        catch (e) {
            done(e);
        }
    }

    formatDescription(callback) {
        let s;
        let description;
        const self=this;
        const checkType=this.value6;
        let typeDescription;
        let totalCourses= this.checkValues.split(',').length;
        if (LangUtils.parseInt(self.value4)!==0) {
            totalCourses = self.value4;
        }
        switch (checkType) {
            case "0":
                //student class must not be found in any previous period
                typeDescription = "must not be found in any of the previous periods";
                break;
            case "-2":
                //student class must be found in the previous period
                typeDescription = "must be found in the previous period";
                break;
            case "2":
                //student class must not be found in the previous period
                typeDescription = "must not be found in the previous period";
                break;
            default:
                //(-1): student class must be found in any previous period
                typeDescription = "must be found in any of the previous periods";
        }
        s = '%s of the following courses %s %s';
        s=this.context.__(s);
        this.courses(function(err,result) {
            if (err) {
                callback(err)
            }
            description = util.format(s, totalCourses, result.map(function(x) { return x.description; }).join(', '),self.context.__(typeDescription));
            callback(null, description);
        });
    }

    courses(callback) {
        try {
            const values = this.checkValues.split(',');
            this.context.model('Course').where('id').in(values).select('id','displayCode','name').silent().all(function(err, result) {
                if (err) { return callback(err); }
                result.forEach(function(x) { x.description = util.format('(%s) %s', x.displayCode, x.name)});
                callback(null, result);
            });
        }
        catch(e) {
            callback(e);
        }
    }
}
