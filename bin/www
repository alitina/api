#!/usr/bin/env node

// check if process was started in development mode
let appModule = (process.env.NODE_ENV === 'development') ? '../server/app' : '../dist/server/app';
let app = require(appModule);
let debug = require('debug')('api:server');
let http = require('http');
let TraceUtils = require('@themost/common').TraceUtils;

const DEFAULT_PORT = '5001';
const DEFAULT_IP = '127.0.0.1';

/**
 * Get port from environment and store in Express.
 */
let port = normalizePort(process.env.PORT || DEFAULT_PORT);
app.set('port', port);

/**
 * Create HTTP server.
 */
let server = http.createServer(app);
/**
 * Get ip from environment and store in Express.
 */
let ip = process.env.IP || DEFAULT_IP;
app.set('ip', ip);
/**
 * Listen on provided port.
 */
server.listen(port, ip);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      // eslint-disable-next-line no-console
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      // eslint-disable-next-line no-console
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  let addr = server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  TraceUtils.debug('Listening on ' + bind);
}
