import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import {INSTITUTE, DEPARTMENT, STUDY_PROGRAM, GRADE_SCALES} from './data';
import Institute from '../server/models/institute-model';
import StudyProgram from '../server/models/study-program-model';
import Department from '../server/models/department-model';
import GradeScale from "../server/models/grade-scale-model";
const executeInTransaction = TestUtils.executeInTransaction;


describe('StudyProgram', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        process.env.NODE_ENV = 'test';
        return done();
    });
    afterAll(done => {
        if (context) {
            return context.finalize(() => done());
        }
        process.env.NODE_ENV = 'development';
        return done();
    });

    it('should create study program', async () => {
        await executeInTransaction(context, async ()=> {
            let institute = INSTITUTE;
            let department = DEPARTMENT;
            await context.model(Institute).silent().insert(institute);
            await context.model(Department).silent().insert(department);
            await context.model(GradeScale).silent().insert(GRADE_SCALES);
            /**
             * @type {StudyProgram}
             */
            let studyProgram = STUDY_PROGRAM;
            // get grade scale
            studyProgram.gradeScale = await context.model(GradeScale)
                .where('name').equal('0-10')
                .silent().getItem();
            await context.model(StudyProgram).silent().insert(studyProgram);
            expect(studyProgram.id).toBeTruthy();
            studyProgram = await context.model(StudyProgram)
                .where('id').equal(studyProgram.id)
                .expand('department')
                .silent()
                .getItem();
            expect(studyProgram.id).toBeTruthy();
            expect(studyProgram.department.id).toBe(department.id);

        });
    });

    it('should update study program', async () => {
        await executeInTransaction(context, async ()=> {
            let institute = INSTITUTE;
            let department = DEPARTMENT;
            await context.model(Institute).silent().insert(institute);
            await context.model(Department).silent().insert(department);
            await context.model(GradeScale).silent().insert(GRADE_SCALES);
            /**
             * @type {StudyProgram}
             */
            let studyProgram = STUDY_PROGRAM;
            // get grade scale
            studyProgram.gradeScale = await context.model(GradeScale)
                .where('name').equal('0 to 10')
                .silent().getItem();
            await context.model(StudyProgram).silent().insert(studyProgram);
            expect(studyProgram.id).toBeTruthy();
            studyProgram = await context.model(StudyProgram)
                .where('id').equal(studyProgram.id)
                .expand('department')
                .silent()
                .getItem();
            expect(studyProgram).toBeTruthy();
            studyProgram.abbreviation = 'MLU';
            await context.model(StudyProgram).silent().save(studyProgram);
            studyProgram = await context.model(StudyProgram)
                .where('id').equal(studyProgram.id)
                .expand('department')
                .silent()
                .getItem();
            expect(studyProgram.abbreviation).toBe('MLU');
        });
    });

    it('should try to delete study program', async () => {
        await executeInTransaction(context, async ()=> {
            let institute = INSTITUTE;
            let department = DEPARTMENT;
            await context.model(Institute).silent().insert(institute);
            await context.model(Department).silent().insert(department);
            await context.model(GradeScale).silent().insert(GRADE_SCALES);
            /**
             * @type {StudyProgram}
             */
            let studyProgram = STUDY_PROGRAM;
            // get grade scale
            studyProgram.gradeScale = await context.model(GradeScale)
                .where('name').equal('0 to 10')
                .silent().getItem();
            await context.model(StudyProgram).silent().insert(studyProgram);
            // remove
            await context.model(StudyProgram).silent().remove(studyProgram);
            // get
            studyProgram = await context.model(StudyProgram)
                .where('id').equal(studyProgram.id)
                .expand('department')
                .silent()
                .getItem();
            expect(studyProgram).toBeTruthy();
        });
    });

});
